﻿using NUnit.Framework;
using ODf.Test.Framework;
using ODf.Test.Framework.Allure;
using Utilities;
using Assert = ODf.Test.Framework.Asserts;

namespace Autotests.NUnit
{
    [TestFixture]
    [AllureSuite("Примеры тестов, показывающие работу хелперов (NUnit)")]
    public class HelpersExamplesTests : SuiteBase
    {
        [Test, AllureTestName("Проверка количества записей"),
            AllureSubSuite("БД"), AllureTags("Позитивный", "БД"),
            AllureTestDescription("Проверка количества записей в базе данных"),
            AllureSeverity(Allure.Commons.SeverityLevel.minor),
            AllureTmsLink("TMS-1"), AllureIssueLink("ISSUE-1")]
        public void TestDatabase()
        {
            var count = Database.GetEmployeesCount();
            Assert.AreEqualAndAccumulate(8, count, "Количество записей", "Неправильное количество записей");

            var employees = Database.GetEmployeesListByCity("Calgary");
            Assert.AreEqualAndAccumulate(5, employees.Count, "Количество записей по городу \"Calgary\"", "Неправильное количество записей");

            var employee = Database.GetEmployeeByName("Robert", "King");
            Assert.AreEqual("IT Staff", employee.Title, "Title", "Неправильный Title");

            employee.FirstName += ContentHelper.GetTimestamp();
            employee.LastName += ContentHelper.GetTimestamp();
            employee.EmployeeId = 0;
            var insertEmployee = Database.InsertEmployee(employee);
            Assert.AreEqualAndAccumulate(1, insertEmployee, "Количество строк, затронутое вставкой в БД",
                "Неправильное количество строк, затронутое вставкой в БД");

            var deleteEmployee = Database.DeleteEmployee(employee);
            Assert.AreEqual(1, deleteEmployee, "Количество строк, затронутое удалением из БД",
                "Неправильное количество строк, затронутое удалением из БД");
        }

        [Test, AllureTestName("Тест с ожиданием выполнения условия")]
        public void TestWait()
        {
            // Получение данных через ожидание выполнения условия
            var timestamp = Wait.For(
                    () => ContentHelper.GetTimestamp().ToString(), // Источник данных. Строка с unix timestamp
                    t => t.EndsWith("4"), // Условие. Строка должна заканчиваться на "4"
                    10, // Время на ожидание
                    500, // Интервал, через который повторяется проверка условия
                    true, // Нужно ли завершить тест и отметить его непройденным, если условие не выполнено
                    "Не удалось получить нужное значение" // Сообщение об ошибке, если условие не выполнено
            );

            // Логирование в консоль и log-файл
            Logger.Info("Получено значение " + timestamp);

            // Проверка
            Assert.IsTrue(
                    timestamp != null && timestamp.EndsWith("4"), // Условие проверки
                    "Полученное значение заканчивается на 4", // Описание проверки
                    "Полученное значение не заканчивается на 4" // Сообщение об ошибке
            );
        }
    }
}
