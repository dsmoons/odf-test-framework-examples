﻿using NUnit.Framework;
using ODf.Test.Framework;
using System;
using System.Collections.Generic;
using Utilities;
using Utilities.Pages;
using Utilities.Services;
using static NUnit.Framework.TestContext;

[assembly: Parallelizable(ParallelScope.All), FixtureLifeCycle(LifeCycle.InstancePerTestCase)]
namespace Autotests.NUnit
{
    [TestFixture]
    public class SuiteBase
    {
        #region Public properties

        public Pages Pages { get; private set; }

        public RestPetstoreClient RestPetstoreClient { get; private set; }

        #endregion

        #region Private properties

        private BaseHelper Base { get; set; }

        #endregion

        [SetUp]
        public void SetUp()
        {
            Base = new BaseHelper.Builder(CurrentContext)
                .SetWebDriverFunc(WebDriverHelper.GetDriver)
                .SetActionForAddAttachments(a => AddTestAttachment(a))
                .SetWebDriverLogsAddToReport()
                .SetAllureEnvironmentParameters(new Dictionary<string, string> { { "Environment", RunsettingsHelper.GetProperty("Environment") },
                    {"User name", Environment.UserName }, { "Machine name", Environment.MachineName } })
                .Build();
            Pages = new Pages(Base);
            RestPetstoreClient = new RestPetstoreClient(RunsettingsHelper.GetProperty("RestPetstoreHost"));
            Base.Start();
        }

        [TearDown]
        public void TearDown()
        {
            Base.Stop();
        }
    }
}
