﻿using ODf.Test.Framework;
using ODf.Test.Framework.Wrappers;

namespace Utilities.Pages.Hq.custom.elements
{
    public class FirstPost : PageBase
    {
        private Element Post { get; }

        private Element Header { get; }
        private Element FirstParagraph { get; }
        private Element Date { get; }
        private Element Next { get; }
        private Element Tags { get; }

        public FirstPost(BaseHelper baseHelper) : base(baseHelper)
        {
            Post = GetElementByXpath("//div[@class='post'][1]", "Первый пост");

            Header = GetElementByXpath(Post, "//h2/a", "Заголовок-ссылка");
            FirstParagraph = GetElementByXpath(Post, "//p", "Первый параграф");
            Date = GetElementByClassName(Post, "data", "Дата");
            Next = GetElementByXpath(Post, "//span[@class='next']/a", "Ссылка 'Читать далее'");
            Tags = GetElementByClassName(Post, "tag", "Теги");
        }

        public void CompareHeaderAndNextLink()
        {
            var id = AllureHelper.StartStep("Сравнение ссылок на пост");
            var headerLink = Header.WaitForExists().GetAttribute("href");
            var nextLink = Next.GetAttribute("href");
            Asserts.AreEqualAndAccumulate(headerLink, nextLink, "Ссылки на пост", "Не совпадают ссылки на пост");
            AllureHelper.StopStep(id);
        }

        public string GetFirstParagraphText() =>
            AllureHelper.RunStep("Получение текста первого параграфа первого поста на главной странице", () => FirstParagraph.GetText());

        public string GetDate() =>
            AllureHelper.RunStep("Получение даты первого поста на главной странице", () => Date.GetText());

        public string GetTags() =>
            AllureHelper.RunStep("Получение тегов первого поста на главной странице", () => Tags.GetText());

        public string GetHeaderText() =>
            AllureHelper.RunStep("Получение текста ссылки-заголовка первого поста на главной странице", () => Header.GetText());

        public void GoToPostPage() =>
            AllureHelper.RunStep("Переход на страницу первого поста", () => Next.Click().WaitFor(element => element.NotExists()));
    }
}
