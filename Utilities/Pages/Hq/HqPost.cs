﻿using ODf.Test.Framework;
using ODf.Test.Framework.Wrappers;

namespace Utilities.Pages.Hq
{
    public class HqPost : PageBase
    {
        private Element PostHeader { get; }

        private Element FirstParagraphText { get; }
        private Element Date { get; }
        private Element Tags { get; }

        public HqPost(BaseHelper baseHelper) : base(baseHelper)
        {
            PostHeader = GetElementByXpath("//div[@class='content']/h2", "Заголовок поста");
            FirstParagraphText = GetElementByClassName("first", "Первый параграф");
            Date = GetElementByClassName("data", "Дата");
            Tags = GetElementByClassName("tag", "Теги");
        }

        public string GetPostHeaderText() =>
            AllureHelper.RunStep("Получение заголовка поста", () => PostHeader.GetText());

        public string GetFirstParagraphText() =>
            AllureHelper.RunStep("Получение текста первого параграфа", () => FirstParagraphText.GetText());

        public string GetDate() => AllureHelper.RunStep("Получение даты поста", () => Date.GetText());

        public string GetTags() => AllureHelper.RunStep("Получение тегов поста", () => Tags.GetText());
    }
}
