﻿using ODf.Test.Framework;
using ODf.Test.Framework.Wrappers;
using Utilities.Pages.Hq.custom.elements;

namespace Utilities.Pages.Hq
{
    public class HqDefault : PageBase
    {
        public FirstPost FirstPost { get; }

        public Element FirstPostLink { get; }

        public Element FirstPostReadLink { get; }

        public HqDefault(BaseHelper baseHelper) : base(baseHelper, RunsettingsHelper.GetProperty("HqUrl", "error"))
        {
            FirstPost = GetPage<FirstPost>();

            FirstPostLink = GetElementByXpath("//div/h2/a", "Ссылка на первый пост");
            FirstPostReadLink = GetElementByXpath("//span[@class='next']/a", "Ссылка 'Читать далее'");
        }
    }
}
