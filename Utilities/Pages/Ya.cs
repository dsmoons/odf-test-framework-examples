﻿using ODf.Test.Framework;
using ODf.Test.Framework.Wrappers;

namespace Utilities.Pages
{
    public class Ya : PageBase
    {
        public Element SearchField { get; }

        public Element SearchButton { get; }

        public Ya(BaseHelper baseHelper) : base(baseHelper, RunsettingsHelper.GetProperty("YaUrl", "error"))
        {
            SearchField = GetElementById("text", "Поле поиска");
            SearchButton = GetElementByXpath("//button[@type='submit']", "Кнопка поиска");
        }

        public void Search(string text) =>
            AllureHelper.RunStep("Поиск текста в поисковой системе", () =>
            {
                SearchField.WaitForExists().SendKeys(text);
                SearchButton.Click();
            });
    }
}
