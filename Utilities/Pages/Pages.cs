﻿using ODf.Test.Framework;

namespace Utilities.Pages
{
    public class Pages : PageHelper
    {
        public Ya Ya { get; }

        public Hq.HqDefault HqDefault { get; }
        public Hq.HqPost HqPost { get; }

        public Pages(BaseHelper baseHelper) : base(baseHelper)
        {
            Ya = GetPage<Ya>();

            HqDefault = GetPage<Hq.HqDefault>();
            HqPost = GetPage<Hq.HqPost>();
        }
    }
}
