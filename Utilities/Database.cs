﻿using ODf.Test.Framework;
using System.Collections.Generic;
using System.Linq;
using Utilities.ORM.Chinook;

namespace Utilities
{
    public class Database
    {
        /*public static List<Employee> GetEmployees(string firstName, string lastName) =>
            AllureHelper.RunStep("Запрос в БД. Получение пользователей по имени", () =>
                    new List<Employee>(
                    // Запрос в БД с помощью Entity Framework
                    new ApplicationDbContext().Employees.Where(s => s.FirstName == firstName && s.LastName == lastName)
                    )// Копирование результатов в список для выхода из контекста
                    .LogResult() // Логирование результатов
                );*/

        public static int GetEmployeesCount() =>
            AllureHelper.RunStep("Запрос в БД. Получение количества записей",
                () => new ApplicationDbContext().Employees.Count().LogResultObject());

        public static List<Employee> GetEmployeesListByCity(string city) =>
            AllureHelper.RunStep("Запрос в БД. Получение пользователей по названию города",
                () => new List<Employee>(new ApplicationDbContext().Employees.Where(e => e.City == city)).LogResult());

        public static Employee GetEmployeeByName(string firstName, string lastName) =>
            AllureHelper.RunStep("Запрос в БД. Получение пользователя по имени",
                () => new ApplicationDbContext().Employees
                    .First(s => s.FirstName == firstName && s.LastName == lastName).LogResultObject());

        public static int InsertEmployee(Employee employee) =>
            AllureHelper.RunStep("Вставка нового пользователя в БД", () =>
            {
                using var context = new ApplicationDbContext();
                context.Employees.Add(employee);
                return context.SaveChanges();
            });

        public static int InsertEmployees(List<Employee> employees) =>
            AllureHelper.RunStep("Вставка новых пользователей в БД", () =>
            {
                using var context = new ApplicationDbContext();
                context.Employees.AddRange(employees);
                return context.SaveChanges();
            });

        public static int DeleteEmployee(Employee employee) =>
            AllureHelper.RunStep("Удаление пользователя из БД", () =>
            {
                using var context = new ApplicationDbContext();
                context.Employees.Remove(employee);
                return context.SaveChanges();
            });

    }
}