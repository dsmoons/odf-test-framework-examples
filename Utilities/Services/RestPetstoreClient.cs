﻿using ODf.Test.Framework.Api;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using Utilities.Services.Rest.Petstore;

namespace Utilities.Services
{
    public class RestPetstoreClient : RestServiceClientHelper
    {
        public RestPetstoreClient(string host) : base(host, true, new KeyValuePair<string, string>("Content-Type", "application/json"))
        {
            var user = new { Login = "Login", Password = "Password" };
            SetAuthenticationHeader(new AuthenticationHeaderValue(AuthenticationSchemes.Basic.ToString(),
                    Convert.ToBase64String(Encoding.UTF8.GetBytes($"{user.Login}:{user.Password}"))));
        }

        public RestResponse<List<Pet>> FindByStatus(string status) =>
            SendGetRequest<List<Pet>>("v2/pet/findByStatus", new Dictionary<string, string> { { "status", status } });

        public RestResponse<List<Pet>> FindByStatus(string status, bool needThrow) =>
            SendGetRequest<List<Pet>>(needThrow, $"v2/pet/findByStatus%$?status={status}"); // Метод с ошибкой

        public RestResponse<Order> Order(Order request, bool needThrow = false) =>
            SendPostRequest<Order>(needThrow, "v2/store/order", request);

        public RestResponse<object> UploadImage(long petId, string file) =>
            SendPostRequestWithMultipart($"v2/pet/{petId}/uploadImage",
                new Dictionary<string, string> { { "additionalMetadata", "pic" } },
                new Dictionary<string, string> { { "Accept", "application/json" } }, file);

        public RestResponse<object> Update(long petId, string name, string status) =>
            SendPostRequestWithFormUrlencoded($"v2/pet/{petId}", new Dictionary<string, string> { { "name", name }, { "status", status } });

        public RestResponse<object> Inventory() => SendGetRequest("v2/store/inventory");

        public RestResponse<object> Login(string username, string password) =>
            SendGetRequest("v2/user/login", new Dictionary<string, string> { { "username", username }, { "password", password } });
    }
}
