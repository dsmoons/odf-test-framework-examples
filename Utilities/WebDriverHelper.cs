﻿using ODf.Test.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;

namespace Utilities
{
    public static class WebDriverHelper
    {
        public static IWebDriver GetDriver() => RunsettingsHelper.GetProperty("Browser") switch
        {
            "Chrome" => GetChromeDriver(false),
            "HeadlessChrome" => GetChromeDriver(true),
            "Firefox" => GetFirefoxDriver(false),
            "HeadlessFirefox" => GetFirefoxDriver(true),
            "Edge" => GetEdgeDriver(false),
            "HeadlessEdge" => GetEdgeDriver(true),
            "InternetExplorer" => GetIeDriver(),
            "SelenoidChrome" => GetSelenoidChrome(),
            "SelenoidFirefox" => GetSelenoidFirefox(),
            "Remote" => RemoteWebDriver(),
            _ => GetChromeDriver(false),
        };

        private static ChromeDriver GetChromeDriver(bool headless) => new(GetChromeOptions(headless));

        private static FirefoxDriver GetFirefoxDriver(bool headless) => new(GetFirefoxOptions(headless));
        //new(AppDomain.CurrentDomain.BaseDirectory, GetFirefoxOptions(headless));

        private static EdgeDriver GetEdgeDriver(bool headless)
        {
            var options = new EdgeOptions
            {
                PageLoadStrategy = PageLoadStrategy.Normal,
                AcceptInsecureCertificates = true,
                UnhandledPromptBehavior = UnhandledPromptBehavior.Dismiss
            };
            if (headless)
            {
                options.AddArgument("-headless");
            }
            return new EdgeDriver(options);
        }

        private static InternetExplorerDriver GetIeDriver()
        {
            var options = new InternetExplorerOptions { PageLoadStrategy = PageLoadStrategy.Normal };
            options.AddAdditionalOption("ignoreZoomSettings", true);
            return new InternetExplorerDriver(options);
        }

        private static RemoteWebDriver GetSelenoidChrome() => GetSelenoidRemoteWebDriver(GetChromeOptions(false));

        private static RemoteWebDriver GetSelenoidFirefox() => GetSelenoidRemoteWebDriver(GetFirefoxOptions(false));

        private static RemoteWebDriver RemoteWebDriver()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("window-size=1920,1080");
            return new RemoteWebDriver(chromeOptions);
        }

        private static RemoteWebDriver GetSelenoidRemoteWebDriver(DriverOptions capabilities)
        {
            var selenoidOptions = new Dictionary<string, object>
            {
                { "enableVNC", true },
                { "enableVideo", true },
                { "screenResolution", "1920x1080x24" },
                { "enableLog", true }
            };
            capabilities.AddAdditionalOption("selenoid:options", selenoidOptions);
            return new RemoteWebDriver(new Uri(RunsettingsHelper.GetProperty("SelenoidHost")), capabilities);
        }

        private static ChromeOptions GetChromeOptions(bool headless)
        {
            var options = new ChromeOptions
            {
                PageLoadStrategy = PageLoadStrategy.Normal,
                AcceptInsecureCertificates = true,
                UnhandledPromptBehavior = UnhandledPromptBehavior.Dismiss
            };
            options.AddUserProfilePreference("safebrowsing.enabled", "false");
            options.AddUserProfilePreference("download.default_directory", @"\downloads");
            if (headless)
            {
                options.AddArguments("headless", "disable-gpu", "--no-sandbox");
            }
            return options;
        }

        private static FirefoxOptions GetFirefoxOptions(bool headless)
        {
            var mimeTypes = "text/csv,application/msexcel,application/x-msexcel,application/excel,application/x-excel," +
                       "application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword," +
                       "application/xml,application/pdf,application/octet-stream";
            var options = new FirefoxOptions
            {
                PageLoadStrategy = PageLoadStrategy.Normal,
                AcceptInsecureCertificates = true,
                UnhandledPromptBehavior = UnhandledPromptBehavior.Dismiss
            };
            options.SetPreference("browser.helperApps.neverAsk.openFile", mimeTypes);
            options.SetPreference("browser.helperApps.neverAsk.saveToDisk", mimeTypes);
            options.SetPreference("browser.helperApps.alwaysAsk.force", false);
            options.SetPreference("browser.download.folderList", 2);
            options.SetPreference("browser.download.manager.showWhenStarting", false);
            options.SetPreference("browser.download.dir", @"\downloads");
            options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
            options.SetPreference("browser.download.manager.focusWhenStarting", false);
            options.SetPreference("browser.download.manager.useWindow", false);
            options.SetPreference("browser.download.manager.showAlertOnComplete", false);
            options.SetPreference("browser.download.manager.closeWhenDone", false);
            options.SetPreference("browser.startup.homepage_override.mstone", "ignore");
            options.SetPreference("startup.homepage_welcome_url.additional", "about:blank");
            options.SetPreference("plugin.scan.Acrobat", "99.0");
            options.SetPreference("plugin.scan.plid.all", false);
            options.SetPreference("plugin.disable_full_page_plugin_for_types", mimeTypes);
            options.SetPreference("pdfjs.disabled", true);
            if (headless)
            {
                options.AddArgument("-headless");
            }
            return options;
        }
    }
}
