﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ODf.Test.Framework;
using ODf.Test.Framework.Allure;
using Assert = ODf.Test.Framework.Asserts;

namespace Autotests.MSTest
{
    [TestClass]
    [AllureSuite("Браузерные тесты (MSTest)"), AllureEpic("Типа эпик")]
    public class UiTests : SuiteBase
    {
        [TestMethod("Проверка поисковой выдачи"), AllureTags("Позитивный", "Браузерный"),
            AllureLink("Ссылка 1", "http://ya.ru"), AllureFeature("Фича MSTest"),
            AllureStory("Стори 1"), AllureOwner("О******в Д.О.")]
        public void CheckYaSearch()
        {
            // Действие:
            Pages.Ya.Open();
            Pages.Ya.Search("selenium");
            // Проверка:
            Assert.IsTrueAndAccumulate(() => Pages.Ya.SearchField.NotExists(), "Есть поле поиска");
        }

        [TestMethod("Проверка первого поста"), AllureTags("Позитивный", "Браузерный"),
            AllureLink("Ссылка 2", "link-1", "link"), AllureFeature("Фича MSTest"),
            AllureStory("Стори 2")]
        public void CheckFirstPost()
        {
            Pages.HqDefault.Open();
            Pages.HqDefault.FirstPost.CompareHeaderAndNextLink();

            var firstPostHeaderOnMainPage = Pages.HqDefault.FirstPost.GetHeaderText();
            var firstPostParagraphTextOnMainPage = Pages.HqDefault.FirstPost.GetFirstParagraphText();
            var firstPostDateOnMainPage = Pages.HqDefault.FirstPost.GetDate();
            var firstPostTagsOnMainPage = Pages.HqDefault.FirstPost.GetTags();
            Pages.HqDefault.FirstPost.GoToPostPage();

            var headerOnPostPage = Pages.HqPost.GetPostHeaderText();
            Assert.AreEqualAndAccumulate(firstPostHeaderOnMainPage, headerOnPostPage,
                    "Заголовок поста", "Не совпадают заголовки");

            var firstParagraphTextOnPostPage = Pages.HqPost.GetFirstParagraphText();
            Assert.AreEqualAndAccumulate(firstPostParagraphTextOnMainPage, firstParagraphTextOnPostPage,
                    "Текст первого параграфа первого поста",
                    "Не совпадает текст первого параграфа первого поста");

            var postDate = Pages.HqPost.GetDate();
            Assert.AreEqualAndAccumulate(firstPostDateOnMainPage, postDate,
                    "Дата первого поста", "Не совпадает дата первого поста");

            var postTags = Pages.HqPost.GetTags();
            Assert.AreEqual(firstPostTagsOnMainPage, postTags,
                    "Теги первого поста", "Не совпадают теги первого поста");
        }

        [TestMethod("Действия с вкладками"), AllureSubSuite("Действия")]
        public void TestOperationsWithTabs()
        {
            Pages.Ya.Open();
            Pages.Ya.GetWindow().OpenNewTab();
            Pages.HqDefault.Open();
            Pages.HqDefault.GetWindow().OpenNewTab();
            Pages.HqDefault.GetWindow().OpenNewTab();
            Pages.HqDefault.Open();
            Pages.HqDefault.GetWindow().OpenNewTab();
            Pages.Ya.GetWindow().SwitchToFirstTab();
            Pages.Ya.GetWindow().SwitchToNextTab();
            Pages.HqDefault.GetWindow().SwitchToNextTab();
            Pages.Ya.GetWindow().SwitchToPreviousTab();
            Pages.HqDefault.GetWindow().SwitchToPreviousTab();
            Pages.Ya.GetWindow().SwitchToLastTab();

            Pages.Ya.Sleep(1000);

            Pages.HqDefault.GetWindow().CloseTabAndSwitchToLast();
            Pages.HqDefault.GetWindow().CloseTabAndSwitchToLast();
            Pages.HqDefault.GetWindow().CloseTabAndSwitchToLast();
            Pages.HqDefault.GetWindow().CloseTabAndSwitchToFirst();

            Pages.Ya.SearchField.AssertThat(element => element.Exists(), "Элемент не найден");
        }

        [TestMethod("Действия с окном браузера"), AllureSubSuite("Действия")]
        public void TestOperationsWithBrowserWindow()
        {
            Pages.Ya.Open();
            Pages.Ya.GetWindow().SetSize(1000, 600);
            Pages.Ya.Sleep(1000);
            Pages.Ya.GetWindow().SetFullscreen();
            Pages.Ya.Sleep(1000);
            Pages.Ya.GetWindow().Minimize();
            Pages.Ya.Sleep(1000);
            Pages.Ya.GetWindow().Maximize();
            Assert.IsTrue(() => Pages.Ya.SearchButton.Exists(), "На странице нет кнопки поиска");
        }

        [TestMethod("Действия с элементами"), AllureSubSuite("Действия")]
        public void TestOperationsWithElements()
        {
            Pages.HqDefault.Open();

            AllureHelper.RunStep("Действия с элементом " + Pages.HqDefault.FirstPostReadLink,
                () => Pages.HqDefault.FirstPostReadLink
                    .WaitForExistsAnd(element => element.GetText() != "")
                    .AssertThat(element => element.Exists(), "Элемент не найден")
                    .AssertThat(element => element.IsEnabled(), "Элемент недоступен")
                    .TakeScreenshot());

            AllureHelper.RunStep("Действия с элементом " + Pages.HqDefault.FirstPostLink,
                () => Pages.HqDefault.FirstPostLink.AssertThat(element => element.Exists(), "Элемент не найден")
                    .ToList()
                    .ForEach(element => element.TakeScreenshot()));
        }

        [TestMethod("Тест сравнения верстки"), AllureSubSuite("Сравнение верстки")]
        [AllureTestDescription("Создание двух скриншотов и их сравнение с помощью ImageHelper")]
        public void TestScreenshotsCompare()
        {
            Pages.Ya.Open();
            Pages.Ya.SearchField.SendKeys(ContentHelper.GetTimestamp().ToString());
            var elementScreenshot1 = Pages.Ya.SearchField.TakeScreenshot();

            Pages.Ya.Refresh();
            Pages.Ya.SearchField.SendKeys(ContentHelper.GetTimestamp().ToString());
            var elementScreenshot2 = Pages.Ya.SearchField.TakeScreenshot();

            ImageHelper.CompareScreenshots(elementScreenshot1, elementScreenshot2, "Элемент с разными данными", false);

            Pages.HqDefault.Open();
            var Pagescreenshot1 = Pages.Ya.GetWindow().TakeScreenshot();

            Pages.HqDefault.Refresh();
            var Pagescreenshot2 = Pages.Ya.GetWindow().TakeScreenshot();

            ImageHelper.CompareScreenshots(Pagescreenshot1, Pagescreenshot2, "Страница после обновления", false);
        }

        [TestMethod("Выполнение javascript и взаимодействие с диалоговыми окнами")]
        public void CheckJavascriptAndAlerts()
        {
            Pages.HqDefault.Open();

            // Вылолнение скрипта
            var userAgent = Pages.HqDefault.GetJavaScriptExecutor().ExecuteScript<string>("return window.navigator.userAgent");
            Assert.IsNotNull(userAgent, "userAgent", "userAgent равно null");

            if (!RunsettingsHelper.GetProperty("Browser").StartsWith("Selenoid"))
            {
                // Прикрепление скрипта для его вызова по id с разными аргументами
                var scriptId = Pages.HqDefault.GetJavaScriptExecutor().PinScript("alert(arguments[0])");

                // Выполнение скрипта по его id
                Pages.HqDefault.GetJavaScriptExecutor().ExecuteScript<object>(scriptId, "argument 1");
                Pages.HqDefault.Sleep(500, false);

                // Подтверждение алерта
                Pages.HqDefault.GetAlert().Accept();

                // Выполнение скрипта по его id с другими аргументами
                Pages.HqDefault.GetJavaScriptExecutor().ExecuteScript<object>(scriptId, "argument 2", 45, "");
                Pages.HqDefault.Sleep(500, false);
                Pages.HqDefault.GetAlert().Accept();

                // Открепление скрипта
                Pages.HqDefault.GetJavaScriptExecutor().UnpinScript(scriptId);
            }

            var text = "argument";
            Pages.HqDefault.GetJavaScriptExecutor().ExecuteScript<object>("prompt(arguments[0])", text);
            Pages.HqDefault.Sleep(500, false);

            // Получение текста из диалогового окна
            var alertText = Pages.HqDefault.GetAlert().GetText();
            Assert.AreEqualAndAccumulate(text, alertText,
                    "Текст диалогового окна", "Неправильный текст диалогового окна");

            // Сообщение в диалоговое окно
            Pages.HqDefault.GetAlert().SendKeys("Сообщение");

            // Отклонение диалогового окна
            Pages.HqDefault.GetAlert().Dismiss();

            Pages.HqDefault.GetJavaScriptExecutor().ExecuteScript<object>("confirm('Вопросы?')");
            Pages.HqDefault.GetAlert().Dismiss();
        }

        /*[TestMethod("Изменение масштаба страницы")]
        public void ChangeZoom()
        {
            Pages.HqDefault.Open();
            for (var i = 5; i <= 510; i += 5)
            {
                Pages.HqDefault.GetWindow().Zoom(i);
                Pages.HqDefault.Sleep(50);
            }
        }*/
    }
}
