﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ODf.Test.Framework;
using System;
using System.Collections.Generic;
using Utilities;
using Utilities.Pages;
using Utilities.Services;

[assembly: Parallelize(Scope = ExecutionScope.MethodLevel)]
namespace Autotests.MSTest
{
    [TestClass]
    public class SuiteBase
    {
        #region Public properties

        public Pages Pages { get; private set; }

        public RestPetstoreClient RestPetstoreClient { get; private set; }

        public TestContext TestContext { get; set; }

        #endregion

        #region Private properties

        private BaseHelper Base { get; set; }

        #endregion

        [TestInitialize]
        public void TestInitialize()
        {
            Base = new BaseHelper.Builder(TestContext)
                .SetWebDriverFunc(WebDriverHelper.GetDriver)
                .SetActionForAddAttachments(TestContext.AddResultFile)
                .SetWebDriverLogsAddToReport()
                .SetAllureEnvironmentParameters(new Dictionary<string, string> { { "Environment", RunsettingsHelper.GetProperty("Environment") },
                    {"User name", Environment.UserName }, { "Machine name", Environment.MachineName } })
                .Build();
            Pages = new Pages(Base);
            RestPetstoreClient = new RestPetstoreClient(RunsettingsHelper.GetProperty("RestPetstoreHost"));
            Base.Start();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            Base.Stop();
        }
    }
}
