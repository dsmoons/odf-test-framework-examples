﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ODf.Test.Framework;
using ODf.Test.Framework.Allure;
using ODf.Test.Framework.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Services.Rest.Petstore;
using Assert = ODf.Test.Framework.Asserts;

namespace Autotests.MSTest
{
    [TestClass]
    [AllureSuite("Примеры тестов по api (MsTest)")]
    public class ApiTests : SuiteBase
    {
        [TestMethod("[REST] Проверка методов сервиса Petstore")]
        public void CheckPetstoreRestService()
        {
            var pets = RestPetstoreClient.FindByStatus("available").ResponseObject;
            Assert.IsNotNull(pets, "Ответ от сервиса", "Ответ от сервиса равен null");
            Assert.IsGreater(pets.Count, 0, "Количество элементов в списке", "Количество записей в ответе равно 0");

            var pet = pets.RandomOrDefault();
            Logger.Info($"Выбран объект{Environment.NewLine}{pet.ObjectToString()}");

            var orderRequest = new Order
            {
                PetId = pet.Id,
                Quantity = 1,
                Id = 0,
                ShipDate = DateTime.Now,
                Complete = true,
                Status = "placed"
            };
            var orderResponse = RestPetstoreClient.Order(orderRequest);
            Assert.IsNotNull(orderResponse.ResponseObject, "Order", "Order равен null");

            Assert.AreEqualAndAccumulate(200, orderResponse.StatusCode,
                    "Код состояния HTTP в ответе от сервиса", "Неправильный код состояния HTTP");

            Assert.IsGreaterAndAccumulate(orderResponse.ResponseObject.Id, 0,
                    "Order.Id", "Order.Id равно 0");
            Assert.AreEqualAndAccumulate(orderRequest.PetId, orderResponse.ResponseObject.PetId,
                    "Order.PetId в запросе и ответе", "Не совпадает Order.PetId в запросе и ответе");
            Assert.AreEqualAndAccumulate(orderRequest.Quantity, orderResponse.ResponseObject.Quantity,
                    "Order.Quantity в запросе и ответе", "Не совпадает Order.Quantity в запросе и ответе");
            Assert.AreEqualAndAccumulate(orderRequest.Complete, orderResponse.ResponseObject.Complete,
                    "Order.Complete в запросе и ответе", "Не совпадает Order.Complete в запросе и ответе");
        }

        [TestMethod("[REST] Проверка ошибки метода сервиса Petstore")]
        public void CheckPetstoreRestServiceError()
        {
            RestResponse<List<Pet>> restResponse = null;
            try
            {
                restResponse = RestPetstoreClient.FindByStatus("available", true);
            }
            catch (RestException exception)
            {
                Assert.Contains("unknown", exception.FullMessage, "ERROR`");
            }
            Assert.IsNull(restResponse, "Ответ от сервиса", "Ответ от сервиса не равен null");
        }

        [TestMethod("[REST] Petstore. Методы с Multipart и FormUrlencoded")]
        public void CheckPetstoreMultipartAndFormUrlencoded()
        {
            var pets = RestPetstoreClient.FindByStatus("available").ResponseObject;
            Assert.IsNotNullAndNotEmpty(pets, "Ответ от сервиса", "Ответ от сервиса равен null или пустой");

            Pet pet = null;
            try
            {
                pet = pets.Where(p => p.Id != null && p.Id > 0).OrderBy(p => p.Id).Skip(10).First();
                Logger.Info($"Выбран объект{Environment.NewLine}{pet.ObjectToString()}");
            }
            catch (Exception exception)
            {
                Assert.Fail(exception, "Не найден подходящий объект для метода UploadImage");
            }

            // Multipart
            var uploadImageResponse = RestPetstoreClient.UploadImage((long)pet.Id, "ico.jpg");
            Assert.AreEqual(200, uploadImageResponse.StatusCode, "Неправильный код");

            // FormUrlencoded
            var updateResponse = RestPetstoreClient.Update((long)pet.Id, "ololoshka", "available");
            Assert.AreEqual(200, updateResponse.StatusCode, "Неправильный код");
        }
    }
}
