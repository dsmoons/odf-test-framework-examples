# ODf Test Framework

Фреймворк для создания функциональных и интеграционных автотестов

- использование MSTest или NUnit для запуска тестов, параллелизации и т.д.
- тестирование web-сервисов (Rest, Soap)
- браузерные тесты (Selenium Webdriver)
- десктопные тесты (WinAppDriver)
- легкий доступ в БД
- мягкие и жесткие assert'ы
- гибкое создание шагов для Allure
- вывод результатов выполнения тестов в лог и отчет Allure
- и многое-многое другое...

```
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|  *  |O| |D| |f|  *  |T| |e| |s| |t|  *  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| * |F| |r| |a| |m| |e| |w| |o| |r| |k| * |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```

***

## История изменений:

***

### Версия 2.0.0 *(07.05.2022)*

Обновлены зависимости. Исправлены ошибки.  
Логирование в файлы по умолчанию выключено. Включение/выключение осуществляется методом `BaseHelper.SetLoggerLogToFiles`.  
Добавлен класс `RunsettingsHelper` для чтения свойств из файла `.runsettings`.  

#### Добавлены новые публичные методы:

- `AllureHelper.RunStep`
- `Asserts.AreEqualByCompare`
- `Asserts.AreEqualByCompareAndAccumulate`
- `Asserts.AreNotEqualByCompare`
- `Asserts.AreNotEqualByCompareAndAccumulate`
- `Asserts.IsNotNullAndNotEmpty`
- `Asserts.IsNotNullAndNotEmptyAndAccumulate`
- `BaseHelper.GetThreadUtil`
- `BaseHelper.SetLoggerLogToConsole`
- `BaseHelper.SetLoggerLogToFiles`
- `BaseHelper.ThreadUtil.CreateObjects`
- `ContentHelper.LogResult`
- `ContentHelper.RandomString`
- `Element.AssertAndAccumulateThat`
- `Element.GetDomAttribute`
- `Element.GetDomProperty`
- `Element.RemoveAttribute`
- `Element.ScrollToBottom`
- `Element.ScrollToTop`
- `Element.SetValue`
- `PageBase.Navigate`
- `PageBase.Frame.SwitchToFrame`
- `PageBase.Frame.SwitchToParentFrame`
- `PageBase.JSExecutor.ExecuteScript`
- `PageBase.JSExecutor.ExecuteScriptAsync`
- `PageBase.JSExecutor.PinScript`
- `PageBase.JSExecutor.UnpinScript`
- `PageBase.Window.GetPageSize`
- `PageBase.Window.GetPageSource`
- `PageBase.Window.GetUrl`
- `RunsettingsHelper.GetBoolProperty`
- `RunsettingsHelper.GetIntProperty`
- `RunsettingsHelper.GetProperty`

#### Удалены методы:

- `PageBase.ExecuteJavaScript`
- `PageBase.GetPageTitle`
- `PageBase.GetUrl`

***

### Версия 1.0.2 *(19.06.2021)*

Обновлены зависимости.  
Исправлены некоторые методы.  

#### Добавлены новые публичные методы:

- `Element.SendPassword`

***

### Версия 1.0.1 *(04.05.2021)*

Атрибуты Allure перенесены в ODf.Test.Framework.Allure.  
Исправлены комментарии к некоторым методам.  

#### Добавлены новые публичные методы:

- `ContentHelper.Swap`
- `PageBase.GetWindow.Zoom`

***

### Версия 1.0.0 *(08.04.2021)*

Фреймворк опубликован в Nuget

***